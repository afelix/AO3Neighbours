# encoding=utf-8
from __future__ import absolute_import

import cPickle
import codecs
from collections import defaultdict
import heapq

from bookmarks import UserBookmarks
from utils import NestedDict


class AO3Neighbours(object):
    neighbours = NestedDict()

    def __init__(self):
        pass

    def get_neighbours(self, username):
        user = UserBookmarks(self.neighbours, username)
        user.parse_bookmarks()

        # self.import_data(username)

        # del self.neighbours['score']
        # del self.neighbours['works']

        counter = defaultdict(int)
        for user, user_data in self.neighbours.iteritems():
            if user == username:
                continue
            # counter[user] = len(user_data['works'])
            counter[user] = user_data['score']

        for user in heapq.nlargest(5, counter, key=counter.get):
            print(u'[+] Username: {0}'.format(user))
            print(u'[*] Score: {0}'.format(self.neighbours[user]['score']))
            print(u'[*] Total works in common: {0}'.format(len(self.neighbours[user]['works'])))
            print(u'')

        self.store_data(username)

    def store_data(self, username):
        with codecs.open(u'neighbours-{0}.pkl'.format(username), 'wb', encoding='utf-8') as output_file:
            pickle_data = cPickle.dumps(self.neighbours, cPickle.HIGHEST_PROTOCOL)
            output_file.write(pickle_data.decode('latin-1'))

    def import_data(self, username):
        with codecs.open(u'neighbours-{0}.pkl'.format(username), 'rb', encoding='utf-8') as input_file:
            self.neighbours = cPickle.loads(input_file.read().encode('latin-1'))


if __name__ == '__main__':
    neighbours = AO3Neighbours()
    neighbours.get_neighbours('afelix')

# encoding=utf-8
import math

import requests
from lxml import etree

from fanworks import BookmarkParser


class UserBookmarks(object):
    base_url = 'http://archiveofourown.org'
    headers = {
        'user-agent': 'AO3Neighbours/0.1 (http://hubsec.eu/bots.html)'
    }
    _n_bookmarks = 0
    _tree = None
    _parser = etree.HTMLParser()

    def __init__(self, neighbours, username):
        self._neighbours = neighbours
        self._works = dict()
        self._prepare(username)
        self._username = username

    def _prepare(self, username):
        r = requests.get(self.base_url + '/users/{0}/bookmarks'.format(username), headers=self.headers)
        self._tree = etree.fromstring(r.text, parser=self._parser, base_url=self.base_url)

        self._n_bookmarks = len(self._tree.xpath('//ol[contains(@class, "bookmark")]/li'))
        if self._n_bookmarks == 20:
            # Multiple pages
            n_bookmarks_heading = self._tree.xpath('//div[@id="main"]/h2[@class="heading"]')[0].text
            self._n_bookmarks = int(
                n_bookmarks_heading[n_bookmarks_heading.find('of') + 3:n_bookmarks_heading.find('Bookmarks') - 1]
            )

    def parse_bookmarks(self):
        for page in self._parse_bookmarks_page():
            bookmarks = page.xpath('//ol[contains(@class, "bookmark")]/li')
            for bookmark in bookmarks:
                try:
                    status = bookmark.xpath('p[@class="status"]')[0]
                except IndexError:
                    # Work unavailable
                    continue

                bookmark_status = status.xpath('a/span')[0].attrib['title']
                bookmark_bookmarks_url = status.xpath('span/a')[0].attrib['href']

                work_data = bookmark.xpath('div[contains(@class, "header")]/h4/a')
                work_url = work_data[0].attrib['href']
                work_title = work_data[0].text
                work_id = work_url[work_url.rfind('/') + 1:]

                try:
                    author = work_data[1].text
                except IndexError:
                    # External bookmark
                    author = work_data[0].tail.split('by\n')[1].strip()

                self._works[work_id] = {
                    'title': work_title,
                    'url': work_url,
                    'author': author,
                    'bookmark_status': bookmark_status
                }

                parser = BookmarkParser(self._neighbours, self.base_url, self.headers)
                parser.parse(bookmark.base + bookmark_bookmarks_url, work_id, bookmark_status)

    def _parse_bookmarks_page(self):
        yield self._tree

        if self._n_bookmarks > 20:
            for i in range(2, int(math.ceil(float(self._n_bookmarks) / 20)) + 1):
                r = requests.get(
                    self.base_url + '/users/{username}/bookmarks?page={page}'.format(username=self._username, page=i),
                    headers = self.headers
                )
                tree = etree.fromstring(r.text, self._parser, base_url=self.base_url)
                yield tree

# encoding=utf-8
import math

import requests
from lxml import etree


class FanWork(object):
    def __init__(self, title, author, url):
        self.title = title
        self.author = author
        self.url = url


class BookmarkParser(object):
    _tree = None
    _parser = etree.HTMLParser()

    def __init__(self, neighbours, base_url, headers):
        self.headers = headers
        self._base_url = base_url
        self._neighbours = neighbours

    def _prepare(self, url):
        r = requests.get(url, headers=self.headers)
        self._tree = etree.fromstring(r.text, self._parser, base_url=self._base_url)

        n_bookmarks = len(self._tree.xpath('//ol[contains(@class, "bookmark")]/li[contains(@class, "user")]'))
        if n_bookmarks == 20:
            # Multiple pages
            n_bookmarks_heading = self._tree.xpath('//div[@id="main"]/h2[@class="heading"]')[0].text
            self._n_bookmarks = int(
                n_bookmarks_heading[n_bookmarks_heading.find('of') + 3:n_bookmarks_heading.find('Bookmarks') - 1]
            )
        return n_bookmarks

    def parse(self, url, work_id, bookmark_type):
        for page in self.parse_page(url):
            bookmarks = page.xpath('//ol[contains(@class, "bookmark")]/li[contains(@class, "user")]')
            for bookmark in bookmarks:
                user = bookmark.xpath('div/h5/a')[0]
                user_name = user.text
                user_url = user.attrib['href']

                work_status = bookmark.xpath('div/p[@class="status"]/a/span')[0].attrib['title']

                if 'score' not in self._neighbours[user_name]:
                    self._neighbours[user_name]['score'] = 0

                if work_status == bookmark_type:
                    self._neighbours[user_name]['score'] += 2
                else:
                    self._neighbours[user_name]['score'] += 1

                if 'works' not in self._neighbours[user_name]:
                    self._neighbours[user_name]['works'] = []

                self._neighbours[user_name]['works'].append({
                    'id': work_id,
                    'status': work_status
                })

                if 'info' not in self._neighbours[user_name]:
                    self._neighbours[user_name]['info'] = {
                        'name': user_name,
                        'url': user_url
                    }

    def parse_page(self, url):
        n_bookmarks = self._prepare(url)

        yield self._tree

        if n_bookmarks > 20:
            for i in range(2, int(math.ceil(float(n_bookmarks) / 20)) + 1):
                r = requests.get(url + '?page={page}'.format(page=i), headers=self.headers)
                tree = etree.fromstring(r.text, self._parser, self._base_url)
                yield tree
